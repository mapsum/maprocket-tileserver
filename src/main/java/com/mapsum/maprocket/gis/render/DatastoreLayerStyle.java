/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.gis.render;

import org.geotools.data.DataStore;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.StyleLayer;
import org.geotools.styling.Style;
import org.opengis.feature.type.Name;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import java.io.IOException;

public class DatastoreLayerStyle extends LayerStyle {

    private DataStore store;
    private String feature;
    private Name geom;

    DatastoreLayerStyle(DataStore store, String feature, Style style) {
        super(style);
        this.store = store;
        this.feature = feature;

        try {
            this.geom = store.getFeatureSource(feature).getSchema().getGeometryDescriptor().getName();
        } catch (IOException e) {
            throw new IllegalStateException(
                "Unable to determine geometry name for feature store: "
                    + "store = " + store
                    + ", feature = " + feature,
                e
            );
        }

    }

    public StyleLayer getLayer(FilterFactory2 filterFactory, ReferencedEnvelope envelope) throws IOException {
        SimpleFeatureSource ssource = store.getFeatureSource(feature);

        Filter bbox = filterFactory.bbox(
            filterFactory.property(geom),
            envelope
        );

        SimpleFeatureCollection sfc = ssource.getFeatures(bbox);

        if (sfc.isEmpty()) {
            return null;
        }

        return new FeatureLayer(ssource.getFeatures(bbox), style);
    }
}

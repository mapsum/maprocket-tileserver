/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.config;

import com.mapsum.maprocket.util.Assert;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.styling.SLD;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Style;

import java.io.File;

public class Styling {

    private String id;
    private String location;

    public String getID() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public Style toStyle(File configBase) {

        File f = new File(location);
        if (!f.isAbsolute()) {
            f = new File(configBase, location);
        }

        Assert.isTrue(f.exists(), "Cannot find style file: " + f.getAbsolutePath());
        Assert.isTrue(f.canRead(), "Cannot read style file: " + f.getAbsolutePath());

        try {
            SLDParser parser = new SLDParser(CommonFactoryFinder.getStyleFactory(), f);
            return SLD.defaultStyle(parser.parseSLD());
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public String toString() {
        return "Styling{" +
            "id='" + id + '\'' +
            ", location='" + location + '\'' +
            '}';
    }
}

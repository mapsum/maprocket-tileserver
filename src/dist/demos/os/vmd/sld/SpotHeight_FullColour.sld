<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  ~ Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
  ~
  ~ Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
  ~ is hereby granted, provided that the above copyright notice and this permission notice appear in all
  ~ copies.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
  ~ INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
  ~ FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  ~ LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ~ ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  -->

<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
    <NamedLayer>
        <Name>OS VectorMap&#174; District - Full Colour style</Name>
        <UserStyle>
            <Title>SpotHeight</Title>
            <Abstract>Ordnance Survey. &#169; Crown copyright 2013.</Abstract>


            <!-- Spot heights -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Spot heights 1:7,560 To 1:9,000</Name>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>9000</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#9D7133</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>2.75</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>HEIGHT</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Arial</CssParameter>
                            <CssParameter name="font-size">12</CssParameter>
                            <CssParameter name="font-weight">bold</CssParameter>
                        </Font>
                        <LabelPlacement>
                            <PointPlacement>
                                <AnchorPoint>
                                    <AnchorPointX>0</AnchorPointX>
                                    <AnchorPointY>0</AnchorPointY>
                                </AnchorPoint>
                                <Displacement>
                                    <DisplacementX>2</DisplacementX>
                                    <DisplacementY>2</DisplacementY>
                                </Displacement>
                            </PointPlacement>
                        </LabelPlacement>
                        <Fill>
                            <CssParameter name="fill">#9D7133</CssParameter>
                        </Fill>
                        <Priority>1000</Priority>
                    </TextSymbolizer>
                </Rule>
                <Rule>
                    <Name>Spot heights 1:9,000 To 1:14,000</Name>
                    <MinScaleDenominator>9000</MinScaleDenominator>
                    <MaxScaleDenominator>14000</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#9D7133</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>2.13</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>HEIGHT</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Arial</CssParameter>
                            <CssParameter name="font-size">12</CssParameter>
                        </Font>
                        <LabelPlacement>
                            <PointPlacement>
                                <AnchorPoint>
                                    <AnchorPointX>0</AnchorPointX>
                                    <AnchorPointY>0</AnchorPointY>
                                </AnchorPoint>
                                <Displacement>
                                    <DisplacementX>2</DisplacementX>
                                    <DisplacementY>2</DisplacementY>
                                </Displacement>
                            </PointPlacement>
                        </LabelPlacement>
                        <Fill>
                            <CssParameter name="fill">#9D7133</CssParameter>
                        </Fill>
                        <Priority>1000</Priority>
                    </TextSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
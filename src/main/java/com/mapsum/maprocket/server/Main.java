/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.server;

import com.mapsum.maprocket.config.Configuration;
import org.glassfish.grizzly.GrizzlyFuture;
import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static final Object MUTEX = new Object();


    public static void main(String[] args) {
        System.out.println("MapRocket Starting: " + new Date());

        if (args.length == 0) {
            System.out.println("No config file specified");
            System.out.println("usage: server <config>");
            System.exit(-1);
        }

        Configuration config = null;
        try {
            config = Configuration.newInstance(args[0]);
        } catch (Exception e) {
            System.out.println("Unable to load configuration: " + e.getMessage());
            LOGGER.error("Unable to load configuration: " + e.getMessage(), e);
            System.exit(-1);
        }

        System.out.println("Running with configuration: " + config.getLocation());


        ServerBuilder builder = new ServerBuilder(config);
        HttpServer server = null;

        try {
            server = builder.build();
            server.start();
        } catch (Exception e) {
            System.out.println("Unable to start server: " + e.getMessage());
            LOGGER.error("Unable to start server: " + e.getMessage(), e);
            System.exit(-1);
        }

        System.out.println("MapRocket Tile Server Started: " + new Date());

        // register hooks
        Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
        synchronized (MUTEX) {
            try {
                MUTEX.wait();
            } catch (InterruptedException e) {
                // ignore
            }
        }

        GrizzlyFuture<HttpServer> future = server.shutdown();
        try {
            future.get(30, TimeUnit.SECONDS);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            LOGGER.warn("Ignored error thrown during shutdown: " + e.getMessage());
        }


        // wait on semaphore for notification from hook
        System.out.println("MapRocket stopped: " + new Date());
        System.out.println();

    }

    private static class ShutdownHandler extends Thread {


        @Override
        public void run() {
            System.out.println("MapRocket stopping: " + new Date());

            synchronized (MUTEX) {
                MUTEX.notifyAll();
            }
        }
    }

}

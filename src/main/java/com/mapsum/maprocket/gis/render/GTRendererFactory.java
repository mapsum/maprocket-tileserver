/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.gis.render;

import com.mapsum.maprocket.pool.TypeCreationException;
import com.mapsum.maprocket.pool.TypeFactory;
import org.geotools.renderer.lite.LabelCache;
import org.geotools.renderer.lite.StreamingRenderer;
import org.glassfish.grizzly.threadpool.GrizzlyExecutorService;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;

import java.awt.RenderingHints;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class GTRendererFactory implements TypeFactory<GTRendererFactory.Renderer> {

    private static final RenderingHints RENDER_HINTS = new RenderingHints(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON
    );

    static {
        RENDER_HINTS.put(
            RenderingHints.KEY_FRACTIONALMETRICS,
            RenderingHints.VALUE_FRACTIONALMETRICS_ON
        );

        RENDER_HINTS.put(
            RenderingHints.KEY_COLOR_RENDERING,
            RenderingHints.VALUE_COLOR_RENDER_QUALITY
        );

        RENDER_HINTS.put(
            RenderingHints.KEY_STROKE_CONTROL,
            RenderingHints.VALUE_STROKE_PURE
        );

        RENDER_HINTS.put(
            RenderingHints.KEY_ALPHA_INTERPOLATION,
            RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY
        );

        RENDER_HINTS.put(
            RenderingHints.KEY_RENDERING,
            RenderingHints.VALUE_RENDER_QUALITY
        );

        RENDER_HINTS.put(
            RenderingHints.KEY_INTERPOLATION,
            RenderingHints.VALUE_INTERPOLATION_BICUBIC
        );
    }

    private static final Map<String, Object> RENDERER_PARAMS = new HashMap<>();

    static {
        RENDERER_PARAMS.put(StreamingRenderer.TEXT_RENDERING_KEY, StreamingRenderer.TEXT_RENDERING_ADAPTIVE);
        RENDERER_PARAMS.put(StreamingRenderer.LINE_WIDTH_OPTIMIZATION_KEY, Boolean.FALSE);
        RENDERER_PARAMS.put(StreamingRenderer.SCALE_COMPUTATION_METHOD_KEY, StreamingRenderer.SCALE_ACCURATE);

        // undocumented but in the code base
        RENDERER_PARAMS.put("renderingBuffer", 500);

        // matches OpenLayers DPI settings
        // see https://github.com/openlayers/openlayers/blob/master/lib/OpenLayers/Util.js
        RENDERER_PARAMS.put(StreamingRenderer.DPI_KEY, 72D);
    }

    private final ExecutorService executors;

    public GTRendererFactory() {
        this(GrizzlyExecutorService.createInstance(
            ThreadPoolConfig.defaultConfig()
                .copy()
                .setCorePoolSize(5)
                .setMaxPoolSize(20)));
    }

    public GTRendererFactory(ExecutorService executors) {
        this.executors = executors;
    }

    @Override
    public Renderer newInstance() throws TypeCreationException {
        Renderer renderer = new Renderer();

        renderer.setThreadPool(executors);
        renderer.setJava2DHints(RENDER_HINTS);
        renderer.setRendererHints(RENDERER_PARAMS);

        return renderer;
    }

    public class Renderer extends StreamingRenderer {

        public LabelCache getLabelCache() {
            return labelCache;
        }

    }
}

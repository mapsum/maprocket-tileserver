<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  ~ Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
  ~
  ~ Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
  ~ is hereby granted, provided that the above copyright notice and this permission notice appear in all
  ~ copies.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
  ~ INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
  ~ FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  ~ LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ~ ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  -->

<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
    <NamedLayer>
        <Name>OS VectorMap&#174; District - Full Colour style</Name>
        <UserStyle>
            <Title>PublicAmenity</Title>
            <Abstract>Ordnance Survey. &#169; Crown copyright 2013.</Abstract>


            <!--  Heritage Sites -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Heritage 1:17,000 to 1:24,986</Name>
                    <MinScaleDenominator>17000</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <ExternalGraphic>
                                <OnlineResource xlink:href="vmdsymbols/FullColour/Heritage.svg" />
                                <Format>image/svg+xml</Format>
                            </ExternalGraphic>
                            <Size>21</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>NAME</ogc:PropertyName>
                            <ogc:Literal></ogc:Literal>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Arial</CssParameter>
                            <CssParameter name="font-style">italic</CssParameter>
                            <CssParameter name="font-size">14</CssParameter>
                        </Font>
                        <LabelPlacement>
                            <PointPlacement>
                                <AnchorPoint>
                                    <AnchorPointX>0</AnchorPointX>
                                    <AnchorPointY>0.5</AnchorPointY>
                                </AnchorPoint>
                                <Displacement>
                                    <DisplacementX>15</DisplacementX>
                                    <DisplacementY>0</DisplacementY>
                                </Displacement>
                            </PointPlacement>
                        </LabelPlacement>
                        <Halo>
                            <Radius>2.75</Radius>
                            <Fill>
                                <CssParameter name="fill">#FFFFFF</CssParameter>
                            </Fill>
                        </Halo>
                        <Fill>
                            <CssParameter name="fill">#D9454A</CssParameter>
                        </Fill>
                        <Priority>1000</Priority>
                        <VendorOption name="spaceAround">2</VendorOption>
                        <VendorOption name="autoWrap">24</VendorOption>
                    </TextSymbolizer>
                </Rule>
                <Rule>
                    <Name>Heritage 1:10,000 to 1:17,000</Name>
                    <MinScaleDenominator>10000</MinScaleDenominator>
                    <MaxScaleDenominator>17000</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <ExternalGraphic>
                                <OnlineResource xlink:href="vmdsymbols/FullColour/Heritage.svg" />
                                <Format>image/svg+xml</Format>
                            </ExternalGraphic>
                            <Size>25</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>NAME</ogc:PropertyName>
                            <ogc:Literal></ogc:Literal>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Arial</CssParameter>
                            <CssParameter name="font-style">italic</CssParameter>
                            <CssParameter name="font-size">16</CssParameter>
                        </Font>
                        <LabelPlacement>
                            <PointPlacement>
                                <AnchorPoint>
                                    <AnchorPointX>0</AnchorPointX>
                                    <AnchorPointY>0.5</AnchorPointY>
                                </AnchorPoint>
                                <Displacement>
                                    <DisplacementX>17</DisplacementX>
                                    <DisplacementY>0</DisplacementY>
                                </Displacement>
                            </PointPlacement>
                        </LabelPlacement>
                        <Halo>
                            <Radius>3</Radius>
                            <Fill>
                                <CssParameter name="fill">#FFFFFF</CssParameter>
                            </Fill>
                        </Halo>
                        <Fill>
                            <CssParameter name="fill">#D9454A</CssParameter>
                        </Fill>
                        <Priority>1000</Priority>
                        <VendorOption name="spaceAround">2</VendorOption>
                        <VendorOption name="autoWrap">24</VendorOption>
                    </TextSymbolizer>
                </Rule>
                <Rule>
                    <Name>Heritage 1:7,560 to 1:10,000</Name>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>10000</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <ExternalGraphic>
                                <OnlineResource xlink:href="vmdsymbols/FullColour/Heritage.svg" />
                                <Format>image/svg+xml</Format>
                            </ExternalGraphic>
                            <Size>26</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>NAME</ogc:PropertyName>
                            <ogc:Literal></ogc:Literal>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Arial</CssParameter>
                            <CssParameter name="font-style">italic</CssParameter>
                            <CssParameter name="font-size">16</CssParameter>
                        </Font>
                        <LabelPlacement>
                            <PointPlacement>
                                <AnchorPoint>
                                    <AnchorPointX>0.5</AnchorPointX>
                                    <AnchorPointY>1</AnchorPointY>
                                </AnchorPoint>
                                <Displacement>
                                    <DisplacementX>0</DisplacementX>
                                    <DisplacementY>-15</DisplacementY>
                                </Displacement>
                            </PointPlacement>
                        </LabelPlacement>
                        <Halo>
                            <Radius>3.25</Radius>
                            <Fill>
                                <CssParameter name="fill">#FFFFFF</CssParameter>
                            </Fill>
                        </Halo>
                        <Fill>
                            <CssParameter name="fill">#D9454A</CssParameter>
                        </Fill>
                        <Priority>1000</Priority>
                        <VendorOption name="spaceAround">2</VendorOption>
                        <VendorOption name="autoWrap">24</VendorOption>
                    </TextSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
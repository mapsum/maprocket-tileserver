# Running the Demo

1. Install the sample data from Mapsum.com ()
2. Edit the configuration to match the installed data
3. Run maprocket  `./maprocket-tileserver font.config.json`
4. Open the <code>viewer.html</code> file
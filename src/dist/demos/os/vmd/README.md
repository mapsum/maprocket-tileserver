# Ordnance Survey VMD Demo - Style of Wight

This demo will run the VMD style sheets from the Ordnance Survey. To run the demo, do the following:

1. Download the SZ tile from Ordnance Survey Opendata (https://www.ordnancesurvey.co.uk/opendatadownload/products.html)
2. Unpack the download file into sz, so you end up with sz/SZ_AdministrativeBoundary.shp etc.
3. Run maprocket  `./maprocket-tileserver vmd.config.json`
4. Open viewer.html in a web browser


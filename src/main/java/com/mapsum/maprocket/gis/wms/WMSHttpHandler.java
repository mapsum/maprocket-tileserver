/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.gis.wms;

import com.mapsum.maprocket.gis.render.GTRendererFactory;
import com.mapsum.maprocket.gis.render.LayerStyle;
import com.mapsum.maprocket.gis.render.MapStyle;
import com.mapsum.maprocket.pool.Pool;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.IllegalFilterException;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.map.StyleLayer;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.threadpool.GrizzlyExecutorService;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;
import org.opengis.filter.FilterFactory2;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class WMSHttpHandler extends HttpHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(WMSHttpHandler.class);

    private final Map<String, CoordinateReferenceSystem> crsCache;
    private final Map<String, MapStyle> styles;
    private final Pool<GTRendererFactory.Renderer> pool;
    private final ExecutorService executor;
    private final FilterFactory2 filterFactory;

    public WMSHttpHandler(Map<String, CoordinateReferenceSystem> crsCache, Map<String, MapStyle> styles) {
        this.crsCache = crsCache;
        this.styles = styles;
        this.pool = new Pool<>(new GTRendererFactory());
        this.filterFactory = CommonFactoryFinder.getFilterFactory2();
        this.executor = GrizzlyExecutorService.createInstance(
            ThreadPoolConfig.defaultConfig()
                .copy()
                .setCorePoolSize(5)
                .setMaxPoolSize(10)
        );
    }


    @Override
    public void service(final Request request, final Response response) throws Exception {
        response.suspend();

        executor.execute(new Runnable() {

            @Override
            public void run() {

                final WMSParams params = new WMSParams(request);

                if (!params.isValid()) {
                    response.setStatus(400);
                    response.setHeader("Warning", "199 " + params.getMissing());
                    response.resume();
                    return;
                }

                CoordinateReferenceSystem crs = crsCache.get(params.getSrs());

                if (crs == null) {
                    response.setStatus(400);
                    response.setHeader("Warning", "199 Unknown CRS " + params.getSrs());
                    response.resume();
                    return;
                }

                MapStyle style = styles.get(params.getStyle());
                if (style == null) {
                    response.setStatus(400);
                    response.setHeader("Warning", "199 Unknown Style " + params.getStyle());
                    response.resume();
                    return;
                }


                MapContent map = new MapContent();
                GTRendererFactory.Renderer renderer = null;
                Graphics2D gr = null;
                try {
                    response.setContentType("image/png");

                    MapViewport viewport = new MapViewport();
                    viewport.setCoordinateReferenceSystem(crs);
                    map.setViewport(viewport);

                    ReferencedEnvelope envelope = new ReferencedEnvelope(params.getBbox(), crs);

                    for (LayerStyle layerStyle : style.getLayerStyles()) {
                        StyleLayer layer = layerStyle.getLayer(filterFactory, envelope);

                        if (layer == null) {
                            continue;
                        }

                        map.addLayer(layer);
                    }


                    renderer = pool.take();
                    renderer.getLabelCache().clear();
                    renderer.setMapContent(map);

                    Rectangle imageBounds = new Rectangle(0, 0, params.getWidth(), params.getHeight());


                    BufferedImage image = null;

                    if (params.getBgcolour() == null) {
                        image = new BufferedImage(imageBounds.width, imageBounds.height, BufferedImage.TYPE_INT_ARGB);
                        gr = image.createGraphics();
                        gr.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0));
                    }
                    else {
                        image = new BufferedImage(imageBounds.width, imageBounds.height, BufferedImage.TYPE_INT_RGB);
                        gr = image.createGraphics();
                        gr.setPaint(Color.getColor(params.getBgcolour(), Color.WHITE));
                    }


                    gr.fill(imageBounds);

                    renderer.paint(gr, imageBounds, envelope);

                    ImageIO.write(image, "png", response.getOutputStream());

                } catch (Exception e) {
                    LOGGER.error("Exception occured rendering tile: " + e.getMessage(), e);
                    response.setStatus(500);
                } finally {
                    map.dispose();

                    if (gr != null) {
                        gr.dispose();
                    }

                    if (renderer != null) {
                        pool.release(renderer);
                    }
                    response.resume();

                }
            }
        });

    }
}

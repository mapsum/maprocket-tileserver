/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.pool;

import com.mapsum.maprocket.util.Assert;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;


public class Pool<T> {

    private final Queue<T> queue;
    private final TypeFactory<T> factory;

    public Pool(TypeFactory<T> factory) {
        this(factory, new ConcurrentLinkedDeque<T>());
    }

    public Pool(TypeFactory<T> factory, Queue<T> queue) {
        Assert.notNull(factory, "Type factory was null");
        Assert.notNull(queue, "Queue reference was null");

        this.queue = queue;
        this.factory = factory;
    }

    public T take() {
        T t = queue.poll();
        if (t == null) {
            t = factory.newInstance();
        }
        return t;
    }

    public void release(T object) {

        if (object == null) {
            return;
        }

        this.queue.offer(object);
    }

}

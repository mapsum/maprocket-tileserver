/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.config;

public class Mapping {

    private String feature;
    private String style;
    private String datastore;
    private String coverage;

    public String getDatastore() {
        return datastore;
    }

    public String getFeature() {
        return feature;
    }

    public String getStyle() {
        return style;
    }

    public String getCoverage() {
        return coverage;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Mapping{ feature='").append(feature).append("', style='").append(style).append('\'');

        if (datastore != null) {
            sb.append(", datastore=").append(datastore).append('\'');
        }

        if (coverage != null) {
            sb.append(", coverage='").append(coverage).append('\'');
        }

        sb.append('}');

        return sb.toString();
    }
}

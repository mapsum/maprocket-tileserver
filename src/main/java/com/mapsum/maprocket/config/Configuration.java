/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapsum.maprocket.util.Assert;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Configuration {

    private Server server;

    private WMS wms;

    private List<Font> fonts;

    private List<Datastore> datastores;

    private List<GridCoverage> grids;

    private List<Styling> stylings;

    private List<Mappings> mappings;

    @JsonIgnore
    private File workingPath;

    @JsonIgnore
    private File location;

    public Server getServer() {
        return server;
    }

    public List<Font> getFonts() {
        if (fonts == null) {
            return Collections.emptyList();
        }

        return fonts;
    }

    public List<Datastore> getDatastores() {
        if (datastores == null) {
            return Collections.emptyList();
        }

        return datastores;
    }

    public List<Styling> getStylings() {
        if (stylings == null) {
            return Collections.emptyList();
        }

        return stylings;
    }

    public List<Mappings> getMappings() {
        if (mappings == null) {
            return Collections.emptyList();
        }

        return mappings;
    }

    public List<GridCoverage> getGrids() {
        if (grids == null) {
            return Collections.emptyList();
        }

        return grids;
    }

    public WMS getWms() {
        return wms;
    }

    public File getWorkingPath() {
        return workingPath;
    }

    public File getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Configuration{" +
            "server=" + server +
            ", wms=" + wms +
            ", fonts=" + fonts +
            ", datastores=" + datastores +
            ", grids=" + grids +
            ", stylings=" + stylings +
            ", mappings=" + mappings +
            '}';
    }

    public static Configuration newInstance(String file) throws IOException {
        File f = new File(file);
        Assert.isTrue(f.exists(), "Configuration file does not exist");
        Assert.isTrue(f.canRead(), "Cannot read configuration file");

        ObjectMapper mapper = new ObjectMapper();

        Configuration config = mapper.readValue(new FileInputStream(f), Configuration.class);
        config.workingPath = f.getParentFile();
        config.location = f;

        return config;

    }
}

<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  ~ Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
  ~
  ~ Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
  ~ is hereby granted, provided that the above copyright notice and this permission notice appear in all
  ~ copies.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
  ~ INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
  ~ FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  ~ LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ~ ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  -->

<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
    <NamedLayer>
        <Name>OS VectorMap&#174; District - Full Colour style</Name>
        <UserStyle>
            <Title>RoadTunnel</Title>
            <Abstract>Ordnance Survey. &#169; Crown copyright 2013.</Abstract>


            <!--  Road Tunnels  -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Road Tunnel 1:16,000 to 1:24,986</Name>
                    <MinScaleDenominator>16000</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <LineSymbolizer>
                        <Geometry>
                            <ogc:Function name="offset">
                                <ogc:PropertyName>the_geom</ogc:PropertyName>
                                <ogc:Literal>-0.0642</ogc:Literal>
                                <ogc:Literal>-0.0642</ogc:Literal>
                            </ogc:Function>
                        </Geometry>
                        <Stroke>
                            <CssParameter name="stroke">#505050</CssParameter>
                            <CssParameter name="stroke-width">0.108</CssParameter>
                            <CssParameter name="stroke-dasharray">3 1.5</CssParameter>
                        </Stroke>
                    </LineSymbolizer>
                    <LineSymbolizer>
                        <Geometry>
                            <ogc:Function name="offset">
                                <ogc:PropertyName>the_geom</ogc:PropertyName>
                                <ogc:Literal>0.0642</ogc:Literal>
                                <ogc:Literal>0.0642</ogc:Literal>
                            </ogc:Function>
                        </Geometry>
                        <Stroke>
                            <CssParameter name="stroke">#505050</CssParameter>
                            <CssParameter name="stroke-width">0.108</CssParameter>
                            <CssParameter name="stroke-dasharray">3 1.5</CssParameter>
                        </Stroke>
                    </LineSymbolizer>
                </Rule>
                <Rule>
                    <Name>Road Tunnel 1:7,560 to 1:16,000</Name>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>16000</MaxScaleDenominator>
                    <LineSymbolizer>
                        <Geometry>
                            <ogc:Function name="offset">
                                <ogc:PropertyName>the_geom</ogc:PropertyName>
                                <ogc:Literal>-0.0642</ogc:Literal>
                                <ogc:Literal>-0.0642</ogc:Literal>
                            </ogc:Function>
                        </Geometry>
                        <Stroke>
                            <CssParameter name="stroke">#505050</CssParameter>
                            <CssParameter name="stroke-width">0.162</CssParameter>
                            <CssParameter name="stroke-dasharray">4 2</CssParameter>
                        </Stroke>
                    </LineSymbolizer>
                    <LineSymbolizer>
                        <Geometry>
                            <ogc:Function name="offset">
                                <ogc:PropertyName>the_geom</ogc:PropertyName>
                                <ogc:Literal>0.0642</ogc:Literal>
                                <ogc:Literal>0.0642</ogc:Literal>
                            </ogc:Function>
                        </Geometry>
                        <Stroke>
                            <CssParameter name="stroke">#505050</CssParameter>
                            <CssParameter name="stroke-width">0.162</CssParameter>
                            <CssParameter name="stroke-dasharray">4 2</CssParameter>
                        </Stroke>
                    </LineSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
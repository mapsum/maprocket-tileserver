# MapRocket Tile Server

A small, lightweight WMS-style tile server written in Java using [Grizzly](http://grizzly.java.net/ "Grizzly") and [GeoTools](http://www.geotools.org/ "GeoTools"). 
It's purpose is for quick deployments and testing as an alternative to Geoserver when the full bells and whistles aren't needed.
 

Issues can be raised on the [issues](https://bitbucket.org/mapsum/maprocket-tileserver/issues "Maprocket Tileserver Issues") page. 

## Requirements

To run you'll need a JVM: [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html "Oracle") or [OpenJDK](http://openjdk.java.net/ "OpenJDK")

To build the product, make sure you have a JDK installed (again see links above).

## Building

The build processes uses Gradle - a gradle wrapper is available as part of the source:


```
#!bash

 $ ./gradlew distZip
```

 
This will build a complete binary distribution. Available tasks can be found using:

  
```
#!bash

$ ./gradlew tasks
```

 
Be warned, that the first build will take a while as the dependencies are resolved and downloaded.
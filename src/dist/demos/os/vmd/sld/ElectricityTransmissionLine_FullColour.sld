<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  ~ Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
  ~
  ~ Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
  ~ is hereby granted, provided that the above copyright notice and this permission notice appear in all
  ~ copies.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
  ~ INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
  ~ FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  ~ LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ~ ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  -->

<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
    <NamedLayer>
        <Name>OS VectorMap&#174; District - Full Colour style</Name>
        <UserStyle>
            <Title>ElectricityTransmissionLine</Title>
            <Abstract>Ordnance Survey. &#169; Crown copyright 2013.</Abstract>


            <!--  ETLs  -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Electricity Transmission Line 1:16,000 to 1:24,986</Name>
                    <MinScaleDenominator>16000</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <LineSymbolizer>
                        <Stroke>
                            <CssParameter name="stroke">#B2B8AD</CssParameter>
                            <CssParameter name="stroke-width">0.23</CssParameter>
                            <CssParameter name="stroke-dasharray">10 4</CssParameter>
                        </Stroke>
                    </LineSymbolizer>
                </Rule>
                <Rule>
                    <Name>Electricity Transmission Line 1:7,560 to 1:16,000</Name>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>16000</MaxScaleDenominator>
                    <LineSymbolizer>
                        <Stroke>
                            <CssParameter name="stroke">#B2B8AD</CssParameter>
                            <CssParameter name="stroke-width">0.345</CssParameter>
                            <CssParameter name="stroke-dasharray">10 4</CssParameter>
                        </Stroke>
                    </LineSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
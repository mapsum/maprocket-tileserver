/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.config;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.jdbc.JDBCDataStore;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Postgis implements Datastore {

    private String host;
    private int port;
    private String schema;
    private String database;
    private String user;
    private String passwd;
    private String id;


    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getSchema() {
        return schema;
    }

    public String getDatabase() {
        return database;
    }

    public String getUser() {
        return user;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getID() {
        return id;
    }

    private Map<String, Object> toParams() {
        Map<String, Object> params = new HashMap<>();
        params.put("dbtype", "postgis");
        params.put("host", host);
        params.put("port", port);
        params.put("schema", schema);
        params.put("database", database);
        params.put("user", user);
        params.put("passwd", passwd);

        return params;
    }

    @Override
    public DataStore toDataStore(File configBase) {
        try {
            DataStore store = DataStoreFinder.getDataStore(toParams());

            if (store instanceof JDBCDataStore) {
                // force population of aggrgate functions - race condition
                // if this isn't populated before rendering occurs
                ((JDBCDataStore)store).getAggregateFunctions();
            }

            return store;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public String toString() {
        return "Postgis{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", schema='" + schema + '\'' +
                ", database='" + database + '\'' +
                ", user='" + user + '\'' +
                ", passwd='" + passwd + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}

/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.gis.wms;

import com.vividsolutions.jts.geom.Envelope;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.util.Parameters;

public class WMSParams {

    private static final String BBOX = "BBOX";
    private static final String SRS = "SRS";
    private static final String WIDTH = "WIDTH";
    private static final String HEIGHT = "HEIGHT";
    private static final String BGCOLOR = "BGCOLOR";
    private static final String STYLE = "STYLE";


    private Envelope bbox;
    private String srs;
    private int width;
    private int height;
    private String format;
    private String bgcolour;
    private String style;

    public WMSParams(Parameters params) {
        String sbox = params.getParameter(BBOX);
        if (sbox != null) {

            String[] parts = sbox.split(",");

            if (parts.length == 4) {
                bbox = new Envelope(
                    Double.parseDouble(parts[0].trim()),
                    Double.parseDouble(parts[2].trim()),
                    Double.parseDouble(parts[1].trim()),
                    Double.parseDouble(parts[3].trim())
                );
            }
        }

        srs = params.getParameter(SRS);
        width = params.getParameter(WIDTH) == null ? 256 : Integer.parseInt(params.getParameter(WIDTH));
        height = params.getParameter(HEIGHT) == null ? 256 : Integer.parseInt(params.getParameter(HEIGHT));
        bgcolour = params.getParameter(BGCOLOR);
        format = "image/png";
        style = params.getParameter(STYLE);
    }


    public WMSParams(Request request) {

        String sbox = request.getParameter(BBOX);
        if (sbox != null) {

            String[] parts = sbox.split(",");

            if (parts.length == 4) {
                bbox = new Envelope(
                        Double.parseDouble(parts[0].trim()),
                        Double.parseDouble(parts[2].trim()),
                        Double.parseDouble(parts[1].trim()),
                        Double.parseDouble(parts[3].trim())
                );
            }
        }

        srs = request.getParameter(SRS);
        width = request.getParameter(WIDTH) == null ? 256 : Integer.parseInt(request.getParameter(WIDTH));
        height = request.getParameter(HEIGHT) == null ? 256 : Integer.parseInt(request.getParameter(HEIGHT));
        bgcolour = request.getParameter(BGCOLOR);
        format = "image/png";
        style = request.getParameter(STYLE);
    }

    public boolean isValid() {
        return bbox != null && srs != null && style != null;
    }

    /**
     * Gets a list of missing parameters - useful if 'isValid()' returns false
     *
     * @return
     */
    public String getMissing() {
        StringBuilder sb = new StringBuilder();
        if (bbox == null) {
            sb.append(BBOX).append(" ");
        }

        if (srs == null) {
            sb.append(SRS).append(" ");
        }

        if (style == null) {
            sb.append(STYLE).append(" ");
        }

        return sb.toString().trim();
    }

    public Envelope getBbox() {
        return bbox;
    }

    public String getSrs() {
        return srs;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getFormat() {
        return format;
    }

    public String getBgcolour() {
        return bgcolour;
    }

    public String getStyle() {
        return style;
    }

    @Override
    public String toString() {
        return "WMSParams{" +
                "bbox=" + bbox +
                ", srs='" + srs + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", format='" + format + '\'' +
                ", bgcolour='" + bgcolour + '\'' +
                '}';
    }
}

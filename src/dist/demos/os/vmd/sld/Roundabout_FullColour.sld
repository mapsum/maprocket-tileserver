<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  ~ Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
  ~
  ~ Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
  ~ is hereby granted, provided that the above copyright notice and this permission notice appear in all
  ~ copies.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
  ~ INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
  ~ FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  ~ LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ~ ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  -->

<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
    <NamedLayer>
        <Name>OS VectorMap&#174; District - Full Colour style</Name>
        <UserStyle>
            <Title>Roundabout</Title>
            <Abstract>Ordnance Survey. &#169; Crown copyright 2013.</Abstract>


            <!-- Roundabouts -->


            <!--  Local Road Fill 1:7,142 to 1:23,607 -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Local Road Fill</Name>
                    <ogc:Filter>
                        <ogc:Or>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>CLASSIFICA</ogc:PropertyName>
                                <ogc:Literal>Local Street</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>CLASSIFICA</ogc:PropertyName>
                                <ogc:Literal>Private Road Publicly Accessible</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:Or>
                    </ogc:Filter>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FEFEFE</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>35.958</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#505050</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>9.598</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FCFCFA</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>7.693</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>


            <!--  Minor Road Fill 1:7,142 to 1:23,607 -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Minor Road Fill</Name>
                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>CLASSIFICA</ogc:PropertyName>
                            <ogc:Literal>Minor Road</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FFF3B5</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>35.958</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#505050</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>9.598</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FCFCFA</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>7.693</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>


            <!--  B Road Fill 1:7,142 to 1:23,607 -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>B Road Fill</Name>
                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>CLASSIFICA</ogc:PropertyName>
                            <ogc:Literal>B Road</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FFC073</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>35.958</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#505050</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>9.598</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FCFCFA</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>7.693</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>

            <!--  A Road Fill 1:7,142 to 1:23,607 -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>A Road Fill</Name>
                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>CLASSIFICA</ogc:PropertyName>
                            <ogc:Literal>A Road</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FF879E</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>35.958</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#505050</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>9.598</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FCFCFA</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>7.693</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>

            <!--  Primary Road Fill 1:7,142 to 1:23,607 -->

            <FeatureTypeStyle>
                <Rule>
                    <Name>Primary Road Fill</Name>
                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>CLASSIFICA</ogc:PropertyName>
                            <ogc:Literal>Primary Road</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>7142</MinScaleDenominator>
                    <MaxScaleDenominator>23607</MaxScaleDenominator>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#37C35B</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>35.958</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#505050</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>9.598</Size>
                        </Graphic>
                    </PointSymbolizer>
                    <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
                        <Graphic>
                            <Mark>
                                <WellKnownName>circle</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#FCFCFA</CssParameter>
                                </Fill>
                            </Mark>
                            <Size>7.693</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.util;

public class Assert {

    private Assert() {
    }

    public static void notNull(Object obj) {
        notNull(obj, null);
    }

    public static void notNull(Object obj, String message) {
        if (obj == null) {
            throw new IllegalArgumentException(message == null ? "Object reference was null" : message);
        }
    }

    public static void hasText(String str) {
        hasText(str, null);
    }

    public static void hasText(String str, String message) {
        if (str == null || str.trim().length() == 0) {
            throw new IllegalArgumentException(message == null ? "String argument was empty/null" : message);
        }
    }

    public static void isTrue(boolean exp) {
        isTrue(exp, null);
    }

    public static void isTrue(boolean exp, String message) {
        if (!exp) {
            throw new IllegalArgumentException(message == null ? "Expression was false" : message);
        }
    }


}

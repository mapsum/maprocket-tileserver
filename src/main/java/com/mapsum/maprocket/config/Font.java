/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.config;


import com.mapsum.maprocket.util.Assert;

import java.io.File;

public class Font {

    private String location;
    private String name;

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public java.awt.Font toFont(File configBase) {
        File f = new File(location);

        if (!f.isAbsolute()) {
            f = new File(configBase, location);
        }

        Assert.isTrue(f.exists(), "Font file does not exist: " + f.getAbsolutePath());
        Assert.isTrue(f.canRead(), "Cannot read font file: " + f.getAbsolutePath());

        try {
            return java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, f);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String toString() {
        return "Font{" +
            "location='" + location + '\'' +
            ", name='" + name + '\'' +
            '}';
    }
}

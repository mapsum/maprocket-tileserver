/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.server;

import com.mapsum.maprocket.config.Configuration;
import com.mapsum.maprocket.config.Datastore;
import com.mapsum.maprocket.config.Font;
import com.mapsum.maprocket.config.GridCoverage;
import com.mapsum.maprocket.config.Mapping;
import com.mapsum.maprocket.config.Mappings;
import com.mapsum.maprocket.config.Styling;
import com.mapsum.maprocket.gis.render.MapStyle;
import com.mapsum.maprocket.gis.wms.WMSHttpHandler;
import com.mapsum.maprocket.util.Assert;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.data.DataStore;
import org.geotools.referencing.CRS;
import org.geotools.renderer.style.FontCache;
import org.geotools.styling.Style;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ServerBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerBuilder.class);


    private final Configuration config;

    public ServerBuilder(Configuration config) {
        if (config == null) {
            throw new IllegalStateException("Configuration object not provided");
        }

        this.config = config;
    }

    public HttpServer build() {

        HttpServer server = new HttpServer();

        server.getServerConfiguration().setJmxEnabled(config.getServer().isJmxEnabled());


        LOGGER.debug("Creating WMS listener: host = {}, port = {}", "0.0.0.0", config.getServer().getPort());
        NetworkListener networkListener = new NetworkListener("wms-listener", "0.0.0.0", config.getServer().getPort());

        LOGGER.debug("Creating transport pool: core pool size = {}, max pool size = {}", 5, 20);
        ThreadPoolConfig threadPoolConfig = ThreadPoolConfig
            .defaultConfig()
            .setCorePoolSize(5)
            .setMaxPoolSize(20);

        networkListener.getTransport().setWorkerThreadPoolConfig(threadPoolConfig);

        server.addListener(networkListener);

        Assert.isTrue(config.getDatastores() != null && !config.getDatastores().isEmpty(), "No datastores specified");
        Assert.isTrue(config.getStylings() != null && !config.getStylings().isEmpty(), "No styles specified");
        Assert.isTrue(config.getMappings() != null && !config.getMappings().isEmpty(), "No mappings specified");

        LOGGER.debug("Building Fonts");
        if (config.getFonts() != null) {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            for (Font font : config.getFonts()) {

                java.awt.Font f = font.toFont(config.getWorkingPath());
                ge.registerFont(f);

                LOGGER.debug("Loaded font: {}", f);
            }

            FontCache.getDefaultInstance().resetCache();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GeoTools fonts: " + FontCache.getDefaultInstance().getAvailableFonts());
            }
        }

        LOGGER.debug("Building datastores");
        Map<String, DataStore> datastores = new HashMap<>();
        for (Datastore store : config.getDatastores()) {

            if (datastores.containsKey(store.getID())) {
                throw new IllegalStateException("Duplicate datastore ID: " + store.getID());
            }

            datastores.put(store.getID(), store.toDataStore(config.getWorkingPath()));

            if (LOGGER.isDebugEnabled()) {
                DataStore ds = datastores.get(store.getID());

                LOGGER.debug("Added datastore");
                LOGGER.debug("Datastore ID: " + store.getID());
                LOGGER.debug("Service Info: " + ds.getInfo());
                try {
                    LOGGER.debug("Type names: " + Arrays.toString(ds.getTypeNames()));
                } catch (IOException e) {
                    // ignore for now as this is purely debug
                }
            }
        }

        LOGGER.debug("Building grid coverages");
        Map<String, GridCoverage2D> grids = new HashMap<>();
        for (GridCoverage grid : config.getGrids()) {
            if (grids.containsKey(grid.getID())) {
                throw new IllegalStateException("Duplicate grid coverage ID: " + grid.getID());
            }

            grids.put(grid.getID(), grid.toGridCoverage(config.getWorkingPath()));

            if (LOGGER.isDebugEnabled()) {
                GridCoverage2D ds = grids.get(grid.getID());

                LOGGER.debug("Added grid coverage");
                LOGGER.debug("Grid ID: " + grid.getID());
                LOGGER.debug("Service Info: " + ds.toString());
            }
        }

        LOGGER.debug("Building SLD styles");
        Map<String, Style> styles = new HashMap<>();
        for (Styling styling : config.getStylings()) {
            if (styles.containsKey(styling.getID())) {
                throw new IllegalStateException("Duplicate style ID: " + styling.getID());
            }

            styles.put(styling.getID(), styling.toStyle(config.getWorkingPath()));
        }


        LOGGER.debug("Building style mappings");
        Map<String, MapStyle> mapStyles = new HashMap<>();
        for (Mappings mappings : config.getMappings()) {
            if (mapStyles.containsKey(mappings.getName())) {
                throw new IllegalStateException("Duplicate mapping ID: " + mappings.getName());
            }

            MapStyle mstyle = new MapStyle(mappings.getName());

            for (Mapping mapping : mappings.getStyles()) {

                if (mapping.getDatastore() != null) {
                    addDatastore(mapping, mstyle, datastores, styles);
                    continue;
                }

                if (mapping.getCoverage() != null) {
                    addCoverage(mapping, mstyle, grids, styles);
                }

            }

            Assert.isTrue(
                !mstyle.getLayerStyles().isEmpty(),
                "No style mappings specified for map style: " + mappings.getName()
            );

            mapStyles.put(mappings.getName(), mstyle);
        }

        LOGGER.debug("Building CRS");
        Map<String, CoordinateReferenceSystem> crss = new HashMap<>();
        for (String c : config.getWms().getCrs()) {

            try {
                crss.put(c, CRS.decode(c));
            } catch (FactoryException e) {
                throw new IllegalStateException("Unable to resolve CRS: " + c, e);
            }

            LOGGER.debug("Loaded CRS: ID = {}, CRS = {}", c, crss.get(c));

        }

        LOGGER.debug("Adding WMS handler '/wms'");
        server.getServerConfiguration().addHttpHandler(new WMSHttpHandler(crss, mapStyles), "/wms");

        return server;
    }

    private void addDatastore(Mapping mapping, MapStyle mstyle, Map<String, DataStore> datastores, Map<String, Style> styles) {
        DataStore store = datastores.get(mapping.getDatastore());
        String feature = mapping.getFeature();
        Style style = styles.get(mapping.getStyle());

        Assert.notNull(store, "Cannot find store for name: " + mapping.getDatastore());
        Assert.hasText(feature, "Null/empty feature name specified");
        Assert.notNull(style, "Cannot find style for name: " + mapping.getStyle());

        mstyle.addDatastoreLayerStyle(store, feature, style);
    }

    private void addCoverage(Mapping mapping, MapStyle mstyle, Map<String, GridCoverage2D> grids, Map<String, Style> styles) {
        GridCoverage2D grid = grids.get(mapping.getCoverage());
        Style style = styles.get(mapping.getStyle());

        Assert.notNull(grid, "Cannot find grid for name: " + mapping.getDatastore());
        Assert.notNull(style, "Cannot find style for name: " + mapping.getStyle());

        mstyle.addGridcoverageLayerStyle(grid, style);
    }
}

/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.config;

import com.mapsum.maprocket.util.Assert;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.io.AbstractGridCoverage2DReader;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.GridFormatFinder;

import java.io.File;

public class GridCoverage {

    private String location;
    private String id;


    public String getID() {
        return id;
    }

    /**
     * @return Grid coverage instance
     * @throws IllegalStateException If the coverage could not be created for some reason
     */
    public GridCoverage2D toGridCoverage(File configBase) throws IllegalStateException {
        Assert.notNull(location, "Grid coverage location was null");

        File image = new File(location);

        if (!image.isAbsolute()) {
            image = new File(configBase, location);
        }

        Assert.isTrue(image.exists(), "Font file does not exist: " + image.getAbsolutePath());
        Assert.isTrue(image.canRead(), "Cannot read font file: " + image.getAbsolutePath());

        try {
            AbstractGridFormat format = GridFormatFinder.findFormat(image);
            AbstractGridCoverage2DReader reader = format.getReader(image);
            return reader.read(null);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String toString() {
        return "GridCoverage{" +
            "location='" + location + '\'' +
            ", id='" + id + '\'' +
            '}';
    }
}

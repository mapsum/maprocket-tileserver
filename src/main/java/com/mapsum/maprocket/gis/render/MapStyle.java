/*
 * Copyright (c) 2012 - 2014, Alex Davies-Moore (alex@mapsum.com)
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 * FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package com.mapsum.maprocket.gis.render;


import com.mapsum.maprocket.util.Assert;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.data.DataStore;
import org.geotools.styling.Style;

import java.util.ArrayList;
import java.util.List;

public class MapStyle {

    private final String name;

    private final List<LayerStyle> layerStyles;

    public MapStyle(String name) {
        this.name = name;
        this.layerStyles = new ArrayList<>();
    }

    public void addGridcoverageLayerStyle(GridCoverage2D grid, Style style) {
        Assert.notNull(grid, "Null coverage reference");
        Assert.notNull(style, "Null style reference");
        layerStyles.add(new GridCoverageLayerStyle(grid, style));
    }

    public void addDatastoreLayerStyle(DataStore store, String feature, Style style) {
        Assert.notNull(store, "Null data store reference");
        Assert.notNull(style, "Null style reference");
        Assert.hasText(feature, "Empty feature name reference");

        layerStyles.add(new DatastoreLayerStyle(store, feature, style));
    }

    public List<LayerStyle> getLayerStyles() {
        return layerStyles;
    }

    public String getName() {
        return name;
    }

}
